<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Aplatir automatiquement les photo au format png en fin de filtre image
 * @param string $img
 * @return string
 */
function image_aplatit_png_post_image_filtrer($img) {
	if (str_contains($img, '.png')) {
		$fichier_ori = $fichier = (extraire_attribut($img, 'src') ?? '');
		if (($p = strpos($fichier, '?')) !== false) {
			$fichier = substr($fichier, 0, $p);
		}
		if (strlen($fichier) < 1) {
			if (strpos($img, '<img ') !== 0) {
				$fichier = $img;
			}
		}
		if (strlen($fichier)) {
			# si jamais le fichier final n'a pas ete calcule car suppose temporaire
			# et qu'il ne s'agit pas d'une URL
			if (strpos($fichier, '://') === false and !@file_exists($fichier)) {
				reconstruire_image_intermediaire($fichier);
			}
			$fichier_alt = _image_aplatit_png_src_trop_grosse($fichier);
			if ($fichier_alt !== $fichier) {
				$img = str_replace($fichier_ori, $fichier_alt, $img);
			}
		}
	}

	return $img;
}

/**
 * Aplatir automatiquement les logos images au format PNG
 * @param array $flux
 * @return array
 */
function image_aplatit_png_recuperer_fond($flux) {
	if ($flux['args']['fond'] === 'modeles/logo') {
		foreach (['logo_on', 'logo_off'] as $logo) {
			if (!empty($flux['args']['contexte'][$logo])
			  and $src = $flux['args']['contexte'][$logo]
			  and str_contains($src, '.png')
			  and str_contains($flux['data']['texte'], $src)
			) {
				$src_alt = _image_aplatit_png_src_trop_grosse($src);
				if ($src_alt !== $src) {
					$flux['data']['texte'] = str_replace($src, $src_alt, $flux['data']['texte']);
				}
			}
		}
	}
	return $flux;
}

/**
 * Tester si le logo est un PNG monstrueux, auquel cas on l'aplatit en JPG
 * (perf issue)
 * On regarde sa densite poids/(largeur x hauteur) pour decider si on garde le PNG ou pas
 *
 * @param string $logo
 * @return string
 */
function _image_aplatit_png_image_trop_grosse($img) {
	if ($img
		and $src = extraire_attribut($img, 'src')) {

		$_image_aplatit_png_src_trop_grosse = charger_filtre('_image_aplatit_png_image_trop_grosse');
		$src_jpg = $_image_aplatit_png_src_trop_grosse($src);
		if ($src_jpg !== $src) {
			$img = str_replace($src, $src_jpg, $img);
		}
	}

	return $img;
}

/**
 * @param string $src
 * @return string
 */
function _image_aplatit_png_src_trop_grosse($src) {
	$src_file = supprimer_timestamp($src);

	if (substr($src_file, -4) === '.png'
		and file_exists($src_file)
		and $t = filesize($src_file)
		and $srcsize = spip_getimagesize($src_file)) {
		// calculer le ratio poids/(largeurxhauteur)
		// au dela de 1.35 on est a priori sur de la photo qu'il faut passer en JPG
		// ratio calcule sur une large icono media
		// mais avec des images en 960*720@x2 ca fait 3Mo potentiellement, donc pas acceptable
		// on coupe donc plus tot et sur une limite max de taille aussi
		$densite = round($t / ($srcsize[0] * $srcsize[1]), 2);
		if ($t > 400000 or $densite > 0.675) {
			if (!function_exists('image_aplatir')) {
				include_spip('filtres/images_transforme');
			}
			// TODO : dans le futur il faudra aplatir en webp
			defined('_IMAGE_APLATIT_PNG_BG_COLOR') || define('_IMAGE_APLATIT_PNG_BG_COLOR', 'ffffff');
			$logo_jpg = image_aplatir($src_file, 'jpg', _IMAGE_APLATIT_PNG_BG_COLOR, 75);
			$src_jpg = extraire_attribut($logo_jpg, 'src');
			// propager le centre image de la source si il existe
			_image_propager_centre_image($src_file, supprimer_timestamp($src_jpg));

			return timestamp($src_jpg);
		}
	}
	return $src;
}

function _image_propager_centre_image($src, $dest) {
	if (defined('_DIR_PLUGIN_CENTRE_IMAGE')) {
		$dir_centre_forces = sous_repertoire(_DIR_IMG, "cache-centre-image");
		$src_md5 = md5(centre_image_preparer_fichier($src));
		$src_json = $dir_centre_forces . $src_md5 . '.json';
		if (file_exists($src_json)) {
			$dest_md5 = md5(centre_image_preparer_fichier($dest));
			$dir_centre_caches = sous_repertoire(_DIR_VAR, "cache-centre-image");
			$dest_json = $dir_centre_caches . substr($dest_md5, 0,1) . '/' . substr($dest_md5, 1,1) . '/' . $dest_md5 . '.json';
			@copy($src_json, $dest_json);
		}
	}
}
